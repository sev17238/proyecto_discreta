/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_chill;

import java.util.Scanner;

/**
 *@author Diego sevilla
 * @author Jose Tejada
 * @version 1.0 
 * Esta clase es la que desencripta.
 * Universidad del Valle de Guatemala
 * Matemática Discreta 
 * 29-10-2018
 */
public class Desencriptado {
    //Instancia de clase encriptado
    Encriptado CripClass;
    /**
     * Inicialización de la clase
     */
    public Desencriptado(){
        CripClass= new Encriptado();
    }
    /**
     * ESte método desencripta con Hill
     * @param plaintext es el texto a desencriptar
     * @param matEnc es la matriz De Encripción
     * @return un string con el texto desencriptado
     */
     public String CipherHillDesencriptado(String plaintext,int[][] matEnc ){
        int count = 0;
        String str9, cipherStr9;
        String ciphertext = "";
        while(plaintext.length() % 9 != 0){ //se chequea que el texto sea multiplo de 9
            plaintext = plaintext + " ";    //de lo contrario se llena de espacions hasta que los sea
        }
        //System.out.println(plaintext);
        for(int i=0;i<plaintext.length();i++){
            count = count +1;
            if(count == 9){
                str9 = plaintext.substring(i-8, i+1);                
                count = 0;
                System.out.println("\n"+str9);
                cipherStr9 = DesencripcionX9(matEnc,str9);
                ciphertext = ciphertext + cipherStr9; //aqui se concatena cada porcion de 9 letras obtenidas del texto
                                                      //original.
            }            
        }
       
        return ciphertext;
    }
     /**
      * ESte método desencripta
      * @param matEnc la matriz de encripción
      * @param str9 la cadena a desecriptar
      * @return un string ya con la desencripcion
      */
    public String DesencripcionX9(int[][] matEnc,String str9) {
        int[][] mat = new int[3][3]; //matriz formada con los numeros de la cadena de 9 digitos
        String strDic = diccionarioStrInt(str9); //las letras se pasan a enteros segun el diccionario
        //Se llenan los valores de la matriz con los que vienen
        String[] strNum = strDic.split(" ");       
        mat[0][0]= Integer.parseInt(strNum[0]);
        mat[1][0]= Integer.parseInt(strNum[1]);
        mat[2][0]= Integer.parseInt(strNum[2]);
        mat[0][1]= Integer.parseInt(strNum[3]);
        mat[1][1]= Integer.parseInt(strNum[4]);
        mat[2][1]= Integer.parseInt(strNum[5]);
        mat[0][2]= Integer.parseInt(strNum[6]);
        mat[1][2]= Integer.parseInt(strNum[7]);
        mat[2][2]= Integer.parseInt(strNum[8]);
        
        //Acá llenamos la nueva matriz que vamos a sacarle la inversa
        double [][] matrizaInvertir = new double[3][3];
        for (int x = 0; x < matrizaInvertir.length;x++) {
			for (int y = 0; y < matrizaInvertir[x].length; y++) {
				 matrizaInvertir[x][y] = matEnc[x][y];
			}   
		}
        
        //UNa vez llenada la matriz a invertir llamamos al método de sacar matriz inversa
        double [][] matrizInversa=matrizInversa(matrizaInvertir);
        
        //Ahora la nueva matriz int 
        int [][] nuevaMatrizEnc= new int[3][3];
        //Pasamos de Double a int en matriz
        for (int x = 0; x < nuevaMatrizEnc.length;x++) {
            for (int y = 0; y < nuevaMatrizEnc[x].length; y++) {
                nuevaMatrizEnc[x][y] = (int) matrizInversa[x][y];
            }   
	}
       
       //Acá le sacamos módulo a la matriz la volvemos positiva 
        for (int x = 0; x < nuevaMatrizEnc.length;x++) {
            for (int y = 0; y < nuevaMatrizEnc[x].length; y++) {
                while(nuevaMatrizEnc[x][y]<0){
                    nuevaMatrizEnc[x][y]=nuevaMatrizEnc[x][y]+29;
                }
            }   
	}
       //Ahora mandamos a multiplicar la inversa con la matriz de datos
        int[][] matP = productoDosMatrices(nuevaMatrizEnc,mat); //matriz producto de la matriz de encripcion y mat   
        int[][] matPmod = new int[3][3]; //matriz en modulo 29
        
        for (int i = 0; i < matP.length; i++) {
            for (int j = 0; j < matP[i].length; j++) {
               //System.out.print(matPmod[i][j] + "\t");
                matPmod[i][j] = matP[i][j] %29;
            }
        }
        mostrarMatriz(matPmod);
        System.out.print('\n');
        
        String ciphertext = "";
        for (int i = 0; i < matP.length; i++) {
            for (int j = 0; j < matP[i].length; j++) {
               //System.out.print(matPmod[i][j] + "\t");
                ciphertext = ciphertext + Integer.toString(matPmod[j][i])+" "; 
            }
        }    
        //System.out.print(ciphertext);
        ciphertext = diccionarioIntStr(ciphertext); //Los numeros en mod 29 se pasan a letras segun el diccionario
        return ciphertext;
    }
    
     
     /**
     * Metodo que imprime la matriz resultante de multiplicar dos matrices con la condicion de que el
     * numero de columnas de la primera coincidad con el numero de filas de a segunda.
     * @param matrizA la primera matriz con la que se operara
     * @param matrizB la segunda matriz con la que se operara
     * @return una matriz int
     */
    public int[][] productoDosMatrices(int[][] matrizA,int[][] matrizB){       
        if(matrizA != null && matrizB != null){
        if(matrizA[0].length==matrizB.length){
            int [][] Mresult = new int[matrizA.length][matrizB[0].length]; 
            for (int i = 0; i<matrizA.length; i++){ 
                for (int j = 0; j < matrizB[0].length; j++) {
                    for (int k = 0; k < matrizA[0].length; k++) {
                        Mresult[i][j] = Mresult[i][j] + matrizA[i][k]*matrizB[k][j];                    
                    }
                }
            }
            return Mresult;
        }else{
            System.out.println("\nEl producto de las matrices ingresadas no se puede realizar porque el numero\n"
                            + "de columnas de la matrizA es diferente del numero de filas de la matrizB.\n");
        }
        }
        return null;
    }    
    
    /**
     * ESte método llena una nueva matriz para usarla luego
     * @return la matriz llena para usarla
     */
    public int[][] llenarMatrizDesencriptado() {
        System.out.print("Porfavor ingrese los valores de la matriz de desencriptado.\nEj. Matriz[columnas][filas]\n");
        Scanner sc = new Scanner(System.in);
        int[][] mat = new int[3][3];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                int a = 0;
                while(a == 0){
                    try{
                        System.out.print("Posicion[" + i + "][" + j + "] = ");
                        mat[i][j] = sc.nextInt();//EL OBJETO DE TIPO SCANNER LEE
                        a = 1;
                    }catch(Exception e){
                        System.out.print("Ingrese algún número por favor.");
                        a = 1;
                    }
                }
            }
        }
        return mat;
    }
    /**
     * Este método muestra cualquier matriz
     * @param mat la matriz a mostrar
     */
     public void mostrarMatriz(int[][] mat) {        
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                System.out.print(mat[i][j] + "\t");
            }
            System.out.println();
        }
    }
     
      /**
     * Este método muestra cualquier matriz
     * @param mat la matriz a mostrar
     */
     public void mostrarMatrizDouble(double[][] mat) {        
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                System.out.print(mat[i][j] + "\t");
            }
            System.out.println();
        }
    }
      /**
     * Metodo que reemplaza las letras de una cadena por numeros segun el diccionario propuesto.
     * @param cadena cadena con letras Ej: "NIÑO"
     * @return la cadena con las letras reemplazadas por numeros 
     */
    String diccionarioStrInt(String cadena){
        String letra;
        String cadenaDic = "";
        for(int i=0;i<cadena.length();i++){
            letra = cadena.substring(i, i+1);            
            if(letra.equals(" ")){
                cadenaDic = cadenaDic + "0 ";
            }else if(letra.equals("A")||letra.equals("a")){
                cadenaDic = cadenaDic + "1 ";
            }else if(letra.equals("B")||letra.equals("b")){
                cadenaDic = cadenaDic + "2 ";
            }else if(letra.equals("C")||letra.equals("c")){
                cadenaDic = cadenaDic + "3 ";
            }else if(letra.equals("D")||letra.equals("d")){
                cadenaDic = cadenaDic + "4 ";
            }else if(letra.equals("E")||letra.equals("e")){
                cadenaDic = cadenaDic + "5 ";
            }else if(letra.equals("F")||letra.equals("f")){
                cadenaDic = cadenaDic + "6 ";
            }else if(letra.equals("G")||letra.equals("g")){
                cadenaDic = cadenaDic + "7 ";
            }else if(letra.equals("H")||letra.equals("h")){
                cadenaDic = cadenaDic + "8 ";
            }else if(letra.equals("I")||letra.equals("i")){
                cadenaDic = cadenaDic + "9 ";
            }else if(letra.equals("J")||letra.equals("j")){
                cadenaDic = cadenaDic + "10 ";
            }else if(letra.equals("K")||letra.equals("k")){
                cadenaDic = cadenaDic + "11 ";
            }else if(letra.equals("L")||letra.equals("l")){
                cadenaDic = cadenaDic + "12 ";
            }else if(letra.equals("M")||letra.equals("m")){
                cadenaDic = cadenaDic + "13 ";
            }else if(letra.equals("N")||letra.equals("n")){
                cadenaDic = cadenaDic + "14 ";
            }else if(letra.equals("O")||letra.equals("o")){
                cadenaDic = cadenaDic + "15 ";
            }else if(letra.equals("P")||letra.equals("p")){
                cadenaDic = cadenaDic + "16 ";
            }else if(letra.equals("Q")||letra.equals("q")){
                cadenaDic = cadenaDic + "17 ";
            }else if(letra.equals("R")||letra.equals("r")){
                cadenaDic = cadenaDic + "18 ";
            }else if(letra.equals("S")||letra.equals("s")){
                cadenaDic = cadenaDic + "19 ";
            }else if(letra.equals("T")||letra.equals("t")){
                cadenaDic = cadenaDic + "20 ";
            }else if(letra.equals("U")||letra.equals("u")){
                cadenaDic = cadenaDic + "21 ";
            }else if(letra.equals("V")||letra.equals("v")){
                cadenaDic = cadenaDic + "22 ";
            }else if(letra.equals("W")||letra.equals("w")){
                cadenaDic = cadenaDic + "23 ";
            }else if(letra.equals("X")||letra.equals("x")){
                cadenaDic = cadenaDic + "24 ";
            }else if(letra.equals("Y")||letra.equals("y")){
                cadenaDic = cadenaDic + "25 ";
            }else if(letra.equals("Z")||letra.equals("z")){
                cadenaDic = cadenaDic + "26 ";
            }else if(letra.equals("!")){
                cadenaDic = cadenaDic + "27 ";
            }else if(letra.equals("?")){
                cadenaDic = cadenaDic + "28 ";
            }else{
                cadenaDic = cadenaDic + "29 ";
            }           
        }
        return cadenaDic;
    }
    /**
     * Metodo que reemplaza los numeros de una cadena por las letras correspondientes segun el diccionario propuesto.
     * @param cadena cadena con numeros Ej: "1 14 23 13 13 22 11 "
     * @return la cadena con los numeros reemplazados por letras
     */
    String diccionarioIntStr(String cadena){
        String letra;
        String cipherText = "";
        String[] cadenaDic = cadena.split(" "); //separamos cada numero en la cadena en un array[] de strings
        for(int i=0;i<cadenaDic.length;i++){
            letra = cadenaDic[i];    
            if(letra.equals("0")){
                cipherText = cipherText + " ";
            }else if(letra.equals("1")){
                cipherText = cipherText + "A";
            }else if(letra.equals("2")){
                cipherText = cipherText + "B";
            }else if(letra.equals("3")){
                cipherText = cipherText + "C";
            }else if(letra.equals("4")){
                cipherText = cipherText + "D";
            }else if(letra.equals("5")){
                cipherText = cipherText + "E";
            }else if(letra.equals("6")){
                cipherText = cipherText + "F";
            }else if(letra.equals("7")){
                cipherText = cipherText + "G";
            }else if(letra.equals("8")){
                cipherText = cipherText + "H";
            }else if(letra.equals("9")){
                cipherText = cipherText + "I";
            }else if(letra.equals("10")){
                cipherText = cipherText + "J";
            }else if(letra.equals("11")){
                cipherText = cipherText + "K";
            }else if(letra.equals("12")){
                cipherText = cipherText + "L";
            }else if(letra.equals("13")){
                cipherText = cipherText + "M";
            }else if(letra.equals("14")){
                cipherText = cipherText + "N";
            }else if(letra.equals("15")){
                cipherText = cipherText + "O";
            }else if(letra.equals("16")){
                cipherText = cipherText + "P";
            }else if(letra.equals("17")){
                cipherText = cipherText + "Q";
            }else if(letra.equals("18")){
                cipherText = cipherText + "R";
            }else if(letra.equals("19")){
                cipherText = cipherText + "S";
            }else if(letra.equals("20")){
                cipherText = cipherText + "T";
            }else if(letra.equals("21")){
                cipherText = cipherText + "U";
            }else if(letra.equals("22")){
                cipherText = cipherText + "V";
            }else if(letra.equals("23")){
                cipherText = cipherText + "W";
            }else if(letra.equals("24")){
                cipherText = cipherText + "X";
            }else if(letra.equals("25")){
                cipherText = cipherText + "Y";
            }else if(letra.equals("26")){
                cipherText = cipherText + "Z";
            }else if(letra.equals("27")){
                cipherText = cipherText + "!";
            }else if(letra.equals("28")){
                cipherText = cipherText + "?";
            }else{
                cipherText = cipherText + "~";
            }               
        }
        return cipherText;
    }
    
    
    /////////////////////////////////////AREA DE MATRIZ INVERSA///////////////////////////
    /**
     * Método principal de la matriz inversa
     * @param matriz una matriz a invertir
     * @return una matriz double invertida
     */
    public double[][] matrizInversa(double[][] matriz) {
    double det=1/determinante(matriz);
    double[][] nmatriz=matrizAdjunta(matriz);
    multiplicarMatriz(det,nmatriz);
    return nmatriz;
}
 /**
  * Este método multiplica una matriz
  * @param n es el número a multiplicar
  * @param matriz la matriz
  */
public void multiplicarMatriz(double n, double[][] matriz) {
    for(int i=0;i<matriz.length;i++)
        for(int j=0;j<matriz.length;j++)
            matriz[i][j]*=n;
}
 /**
  * ESte método saca la adjunta
  * @param matriz la matriz
  * @return una matriz adjunta
  */
public  double[][] matrizAdjunta(double [][] matriz){
    return matrizTranspuesta(matrizCofactores(matriz));
}

 /**
  * Este método saca la matriz de cofactores
  * @param matriz una matriz
  * @return la matriz de cofactores
  */
public double[][] matrizCofactores(double[][] matriz){
    double[][] nm=new double[matriz.length][matriz.length];
    for(int i=0;i<matriz.length;i++) {
        for(int j=0;j<matriz.length;j++) {
            double[][] det=new double[matriz.length-1][matriz.length-1];
            double detValor;
            for(int k=0;k<matriz.length;k++) {
                if(k!=i) {
                    for(int l=0;l<matriz.length;l++) {
                        if(l!=j) {
                        int indice1=k<i ? k : k-1 ;
                        int indice2=l<j ? l : l-1 ;
                        det[indice1][indice2]=matriz[k][l];
                        }
                    }
                }
            }
            detValor=determinante(det);
            nm[i][j]=detValor * (double)Math.pow(-1, i+j+2);
        }
    }
    return nm;
}
 /**
  * Este método saca la matriz Transpuesta
  * @param matriz la matriz
  * @return una matriz double
  */
public double[][] matrizTranspuesta(double [][] matriz){
    double[][]nuevam=new double[matriz[0].length][matriz.length];
    for(int i=0; i<matriz.length; i++)
    {
        for(int j=0; j<matriz.length; j++)
            nuevam[i][j]=matriz[j][i];
    }
    return nuevam;
}
 /**
  * Este método saca la determinante
  * @param matriz una matriz double
  * @return 
  */
public double determinante(double[][] matriz)
{
    double det;
    if(matriz.length==2)
    {
        det=(matriz[0][0]*matriz[1][1])-(matriz[1][0]*matriz[0][1]);
        return det;
    }
    double suma=0;
    for(int i=0; i<matriz.length; i++){
    double[][] nm=new double[matriz.length-1][matriz.length-1];
        for(int j=0; j<matriz.length; j++){
            if(j!=i){
                for(int k=1; k<matriz.length; k++){
                int indice=-1;
                if(j<i)
                indice=j;
                else if(j>i)
                indice=j-1;
                nm[indice][k-1]=matriz[j][k];
                }
            }
        }
        if(i%2==0)
        suma+=matriz[i][0] * determinante(nm);
        else
        suma-=matriz[i][0] * determinante(nm);
    }
    return suma;
}
    
    //FIN AREA DE MATRIZ INVERSA////////////////////////////////////////////////
}
