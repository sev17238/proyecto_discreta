/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_chill;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Scanner;


/**
 *
 * @author SDiego
 * @author  José Tejada
 */
public class Main_deCipher {
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Desencriptado des = new Desencriptado();
        
        Scanner scan = new Scanner(System.in);
        String readpath, createpath, txtcontent;
        System.out.print("Ingrese la ubicacion del archivo .txt: ");
        readpath = scan.nextLine();
        
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;  
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File (readpath);
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            // Lectura del fichero
            String linea;
            String lineaL = "";
         
            while((linea=br.readLine())!=null){
                //System.out.println(linea);
                lineaL = lineaL + linea;
            }
            //System.out.println(lineaL); 
            int[][] matEnc = des.llenarMatrizDesencriptado();
            System.out.println("La matriz de desencriptado es: ");
            des.mostrarMatriz(matEnc);
            //Acá llenamos la nueva matriz que vamos a sacarle la inversa
            double [][] matrizVerificar = new double[3][3];
            for (int x = 0; x < matrizVerificar.length;x++) {
                for (int y = 0; y < matrizVerificar[x].length; y++) {
                    matrizVerificar[x][y] = matEnc[x][y];
		}   
            }
            
            if (des.determinante(matrizVerificar)==0) {
                System.out.println("-------------------------------------------------------------------------------------------------------");
                System.out.println("NO HAGA ESTO...ESA MATRIZ NO TIENE INVERSA FIJO NO LE VA A SALIR. MATE EL PROGRAMA E INTENTE DE NUEVO");
                System.out.println("-------------------------------------------------------------------------------------------------------");
            }
            
            txtcontent = des.CipherHillDesencriptado(lineaL,matEnc); //se realiza el encriptado del contenido del .txt
            System.out.println("EL TEXTO DESCIFRADO ES:---------------------------------");
            System.out.print(txtcontent);
            System.out.print("\nIngrese la ubicacion donde desea crear el filename.txt: ");
            //si no se especifica una ruta el archivo se creara en la carpeta de proyecto
            createpath = scan.nextLine();
            File file = new File(createpath);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(txtcontent);
            bw.close();
            
        }catch(Exception e){
            System.out.println("UHHHHHHH!!! Hubo un error leyendo la ruta del fichero o algo más. Intente de nuevo");
        }finally{
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try{                    
                if( null != fr ){   
                    fr.close();     
                }                  
            }catch (Exception e2){ 
                System.out.println("UHHHHHHH!!! Hubo un error. Intente de nuevo");
            }
        }
 
    }
    
}

