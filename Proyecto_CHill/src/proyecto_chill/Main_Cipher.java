/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_chill;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Scanner;


/**
 * 
 * @author SDiego
 */
public class Main_Cipher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Encriptado enc = new Encriptado();
        
        Scanner scan = new Scanner(System.in);
        String readpath, createpath, txtcontent;
        System.out.print("Ingrese la ubicacion del archivo .txt: ");
        readpath = scan.nextLine();
        
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;  
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File (readpath);
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            // Lectura del fichero
            String linea;
            String lineaL = "";
         
            while((linea=br.readLine())!=null){
                //System.out.println(linea);
                lineaL = lineaL + linea;
            }
            //System.out.println(lineaL); 
            int[][] matEnc = enc.llenarMatrizEncriptado();
            System.out.print("Su matriz de encriptado es: \n");
            enc.mostrarMatriz(matEnc);
            txtcontent = enc.CipherHill(lineaL,matEnc); //se realiza el encriptado del contenido del .txt
            System.out.print(txtcontent);
            System.out.print("\nIngrese la ubicacion donde desea crear el filename.txt: ");
            //si no se especifica una ruta el archivo se creara en la carpeta de proyecto
            createpath = scan.nextLine();
            File file = new File(createpath);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(txtcontent);
            bw.close();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try{                    
                if( null != fr ){   
                    fr.close();     
                }                  
            }catch (Exception e2){ 
                e2.printStackTrace();
            }
        }
 
    }
    
}
